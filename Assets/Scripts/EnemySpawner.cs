﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Assets.Scripts
{
    [System.Serializable]
    public struct SpawnArea
    {
        public Vector2 X;
        public Vector2 Y;
        public Vector2 Z;
    }
    public class EnemySpawner : MonoBehaviour
    {
        class Respawn
        {
            public Enemy Enemy { get; private set; }
            public float DeltaTime { get; set; }

            public Respawn(Enemy enemy, float deltaTime)
            {
                DeltaTime = deltaTime;
                Enemy = enemy;
            }
        }
        public static EnemySpawner Instance
        {
            get { return m_instance; }
        }

        public GameObject EnemyPrefab;
        public SpawnArea SpawnArea;
        [Range(0, 1000)]
        public int SpawnAtStart = 30;
        private static EnemySpawner m_instance;
        private List <Respawn> m_respawnables;
        private List<Respawn> m_respawnablesRemove;

        void Awake()
        {
            m_instance = this;
            m_respawnables = new List <Respawn>();
            m_respawnablesRemove = new List<Respawn>();
        }

        void Start()
        {
            spawnEnemies();
        }

        void Update()
        {
            checkForRespawn();
        }

        private void checkForRespawn()
        {
            if (m_respawnables.Count == 0) return;
            
            for (int i = 0; i < m_respawnables.Count; ++i)
            {
                m_respawnables[i].DeltaTime += Time.deltaTime;
                if (m_respawnables[i].DeltaTime >= m_respawnables[i].Enemy.RespawnParameter.Time &&
                    isPlayerDistanceBigEnough(m_respawnables[i].Enemy)) // case 1 & 3
                {
                    spawnEnemy(m_respawnables[i].Enemy.SpawnPosition);
                    m_respawnablesRemove.Add(m_respawnables[i]);
                }
            }

            if (m_respawnablesRemove.Count == 0) return;
            foreach (Respawn t in m_respawnablesRemove)
                m_respawnables.Remove(t);
            m_respawnablesRemove.Clear();
        }

        private bool isPlayerDistanceBigEnough(Enemy enemy)
        {

            var playerDistance = Vector3.Distance(enemy.SpawnPosition, Player.GetPlayerPosition());
            return playerDistance >= enemy.RespawnParameter.Distance;
        }

        public void AddRespawnable(Enemy enemy)
        {
            if(isPlayerDistanceBigEnough(enemy)) // case 2
                spawnEnemy(enemy.SpawnPosition);
            else
                m_respawnables.Add(new Respawn(enemy, 0));
        }

        private void spawnEnemies()
        {
            Vector3 spawnPos = Vector3.zero;
            for (int i = 0; i < SpawnAtStart; ++i)
            {
                spawnPos.x = Random.Range(SpawnArea.X.x, SpawnArea.X.y);
                spawnPos.y = Random.Range(SpawnArea.Y.x, SpawnArea.Y.y);
                spawnPos.z = Random.Range(SpawnArea.Z.x, SpawnArea.Z.y);
                GameObject go = spawnEnemy(spawnPos);
                if(!isPlayerDistanceBigEnough(go.GetComponentInChildren<Enemy>(true)))
                    PoolManager.Destroy(go.gameObject);
            }
        }
        
        private GameObject spawnEnemy(Vector3 spawnPos)
        {
            GameObject go = PoolManager.Instantiate(EnemyPrefab, spawnPos);
            go.transform.SetParent(this.transform);
            return go;
        }
    }
}