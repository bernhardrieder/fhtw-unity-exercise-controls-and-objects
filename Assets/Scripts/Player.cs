﻿/* Aufgabe
 * Avatar: Bewegen, Ausrichten, Projektile feuern, Attribute: HP
 */

using UnityEngine;

namespace Assets.Scripts
{
    public class Player : MonoBehaviour
    {
        public GameObject ProjectilePrefab;
        public Transform ProjectileExit;
        public bool IsGameOverPossible = false;
        [Range(0f, 200f)]
        public float HealthPoints = 100f;
        [Range(0f, 10f)]
        public float ShootPauseInSeconds = 0.2f;

        private float m_currentShotWait = 0.5f;
        private float m_maxHealth;
        private static Player m_instance;

        void Awake()
        {
            m_instance = this;
            m_maxHealth = HealthPoints;
            m_currentShotWait = ShootPauseInSeconds;
        }

        void Update()
        {
            m_currentShotWait += Time.deltaTime;
            if (Input.GetKey(KeyCode.Mouse0) && m_currentShotWait >= ShootPauseInSeconds)
            {
                GameObject go = PoolManager.Instantiate(ProjectilePrefab, ProjectileExit.position, ProjectileExit.rotation);
                go.GetComponent<Projectile>().Fire(ProjectileExit.forward, ForceMode.Impulse);
                m_currentShotWait = 0;
            }   
        }

        void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.tag == "Projectile")
            {
                Projectile projectile = collision.gameObject.GetComponent <Projectile>();
                HealthPoints -= projectile.Damage;
                if (HealthPoints <= 0 && IsGameOverPossible)
                    StaticData.IsGameOver = true;
                else if (HealthPoints <= 0 && !IsGameOverPossible)
                    HealthPoints = m_maxHealth;
            }
        }

        public static Vector3 GetPlayerPosition()
        {
            return m_instance.transform.position;
        }
    }
}
