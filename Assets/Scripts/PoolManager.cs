﻿/* Aufgabe
 * Poolmanager verwaltet (initialisieren, aktivieren, deaktivieren)
 *      - Gegner
 *      - Projektile
 */

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public struct PoolingElement
    {
        public GameObject Prefab;
        public int PoolSize;
        public bool ReuseOldestObjectIfPoolDepleted;
    }

    public class PoolManager : MonoBehaviour
    {
        internal struct Pool
        {
            internal Pool(int poolSize, bool reuse)
            {
                Objects = new Stack<GameObject>();
                ActiveObjects = new List<GameObject>();
                Poolsize = poolSize;
                ReuseOldestObjectIfPoolDepleted = reuse;
            }
            internal Pool(int poolSize)
            {
                Objects = new Stack<GameObject>();
                ActiveObjects = new List <GameObject>();
                Poolsize = poolSize;
                ReuseOldestObjectIfPoolDepleted = false;
            }

            public Stack<GameObject> Objects;
            public List<GameObject> ActiveObjects;
            public int Poolsize;
            public bool ReuseOldestObjectIfPoolDepleted;
        }

        public Transform PoolLocation;

        [Header("Pooled Objects")]
        public PoolingElement[] PoolElements;


        private static Dictionary<string, Pool> m_pools;
        private static Transform m_objectPool;

        private void Awake()
        {
            m_objectPool = PoolLocation;
            initPools();
        }

        private void initPools()
        {
            m_pools = new Dictionary <string, Pool>();
            addToPool(PoolElements);
        }

        private static string getCloneName(string originalName)
        {
            originalName += " (clone)";
            return originalName;
        }

        private void addToPool(PoolingElement[] elements)
        {
            GameObject go;
            string name;

            foreach (var e in elements)
            {
                if (e.Prefab == null)
                {
                    Debug.LogError("null ref in poolmanager");
                    continue;
                }

                name = getCloneName(e.Prefab.name);

                if (!m_pools.ContainsKey(name))
                {
                    m_pools.Add(name, new Pool(e.PoolSize, e.ReuseOldestObjectIfPoolDepleted));
                }

                for (int i = 0; i <= e.PoolSize; ++i)
                {
                    go = GameObject.Instantiate(e.Prefab);
                    go.name = name; // because unity name it "... (clone) (clone)" or something like that
                    go.transform.SetParent(m_objectPool.transform);
                    go.SetActive(false);
                    m_pools[go.name].Objects.Push(go);
                }
            }
        }
        public static GameObject Instantiate(GameObject original)
        {
            return Instantiate(original, original.transform.position, original.transform.rotation);
        }

        public static GameObject Instantiate(GameObject original, Vector3 position)
        {
            return Instantiate(original, position, original.transform.rotation);
        }

        public static GameObject Instantiate(GameObject original, Vector3 position, Quaternion rotation)
        {
            
            GameObject go;
            string name = getCloneName(original.name);

            if (m_pools.ContainsKey(name) && m_pools[name].Objects.Count > 1)
            {
                //Debug.Log("Instantiate: " + name);
                go = m_pools[name].Objects.Pop();
                m_pools[name].ActiveObjects.Add(go);
                go.transform.position = position;
                go.transform.rotation = rotation;
                go.SetActive(true);
                return go;
            }

            if (m_pools.ContainsKey(name) && m_pools[name].Objects.Count == 1 && !m_pools[name].ReuseOldestObjectIfPoolDepleted)
            {
                Debug.Log("pool size too small: " + name);

                go = GameObject.Instantiate(m_pools[name].Objects.Peek());
                m_pools[name].ActiveObjects.Add(go);
                go.name = name; // because unity would name it "... (clone) (clone)" or something like that
                go.transform.position = position;
                go.transform.rotation = rotation;
                go.SetActive(true);
                return go;
            }
            if (m_pools.ContainsKey(name) && m_pools[name].Objects.Count == 1 && m_pools[name].ReuseOldestObjectIfPoolDepleted)
            {
                Debug.Log("pool size too small: " + name + " - reuse oldest object");
                Destroy(m_pools[name].ActiveObjects[0]);
                return Instantiate(original, position, rotation);
            }
        
            Debug.LogError("object not in pool: " + name);
            m_pools.Add(name, new Pool(2));
            go = GameObject.Instantiate(original, position, rotation) as GameObject;
            Destroy(go);
            return Instantiate(original, position, rotation);
        
        }

        public static void Destroy(GameObject go)
        {
            go.SetActive(false);
            if (m_pools.ContainsKey(go.name))
            {
                go.transform.SetParent(m_objectPool.transform);
                m_pools[go.name].Objects.Push(go);
                m_pools[go.name].ActiveObjects.Remove(go);
            }
            else
            {
                GameObject.Destroy(go);
            }
        }
    }
}