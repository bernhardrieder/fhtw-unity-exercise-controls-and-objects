﻿using UnityEngine;

namespace Assets.Scripts
{
    public class EnemyAttractionCollider : MonoBehaviour
    {

        public SphereCollider SphereCollider;
        
        public bool FollowPlayer
        {
            get { return m_followPlayer; }
        }
        public GameObject Player
        {
            get { return m_player; }
        }

        public System.Action OnAttractionGained;

        private bool m_followPlayer;
        private GameObject m_player;

        void Awake()
        {
            m_followPlayer = false;
            m_player = null;
        }

        public void SetRadius(float radius)
        {
            SphereCollider.radius = radius;
        }

        public void Reset()
        {
            m_followPlayer = false;
            m_player = null;
        }
        //void OnTriggerEnter(Collider collider)
        //{
        //    if (collider.transform.tag == "Player")
        //    {
        //        m_followPlayer = true;
        //        m_player = collider.gameObject;
        //    }
        //}

        void OnTriggerStay(Collider collider)
        {
            if (!m_followPlayer && collider.transform.tag == "Player")
            {
                m_followPlayer = true;
                m_player = collider.gameObject;
                if (OnAttractionGained != null)
                    OnAttractionGained();
            }
        }

        //void OnTriggerExit(Collider collider)
        //{
        //    if (collider.transform.tag == "Player")
        //    {
        //        Reset()
        //    }
        //}
    }
}
