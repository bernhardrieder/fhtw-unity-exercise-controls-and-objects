﻿/* Aufgabe
 * 
 * Projektile: Attribute: DMG, Speed
 *      - Werden nach einer bestimmten Zeit/Entfernung oder wenn dass Kontingent erschöpft ist aus der Szene „entfernt“
 */

using UnityEngine;

namespace Assets.Scripts
{
    [System.Serializable]
    public struct SpawnParameter
    {
        public float Time;
        public float Distance;
    }
    public class Projectile : MonoBehaviour
    {
        public Rigidbody Body;

        [Header("Parameter:")]
        [Range(0f,100f)]
        public float Damage  = 1f;
        [Range(0f,100f)]
        public float Speed = 1f;
        public SpawnParameter DespawnParameter;

        private float m_lifeTime;
        private Vector3 m_spawnPos;
        private bool m_fired;

        void Awake()
        {
            reset();
        }
        
        void Update()
        {
            if (m_fired)
            {
                Vector3 distanceVector = this.transform.position - m_spawnPos; // OR USE Vector3.Distance(Vector3 a, Vector3 b)
                m_lifeTime += Time.deltaTime;
                if (m_lifeTime >= DespawnParameter.Time || distanceVector.magnitude >= DespawnParameter.Distance)
                {
                    PoolManager.Destroy(this.gameObject);
                }
            }
        }

        void OnDisable()
        {
            reset();
        }
        
        public void Fire(Vector3 direction, ForceMode forceMode)
        {
            m_lifeTime = 0;
            m_spawnPos = this.transform.position;
            m_fired = true;
            Body.AddForce(direction.normalized * Speed, forceMode);
        }

        private void reset()
        {
            m_lifeTime = 0;
            m_spawnPos = Vector3.zero;
            m_fired = false;
            Body.velocity = Vector3.zero;
        }

    }
}