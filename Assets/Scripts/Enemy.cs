﻿/* Aufgabe
 * 
 * Gegner: Attribute: HP, Radius,
 *      - Stationär, haben Aktionsradius, folgen Avatar nach erstem Kontakt
 *      - Halten dann Abstand und feuern Projektile in Richtung Avatar
 *      - Respawn nach Zeit/Entfernung zum Avatar am Ursprung
 */
 
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts
{
    public class Enemy : MonoBehaviour
    {
        public GameObject ProjectilePrefab;
        public Transform ProjectileExit;
        public EnemyAttractionCollider AttractionCollider;
        public Material HeadMaterial;

        [Header("Parameter:")]
        [Range(0f, 100f)]
        public float HealthPoints = 100f;
        [Range(0f, 1000f)]
        public float AttractionRadius = 10f;
        [Range(0f, 100f)]
        public float MaximumProximityDistance = 10f;
        [Range(0f, 100f)]
        public float MovementSpeed = 10f;
        [Range(0f, 10f)]
        public float ShootPauseInSeconds = 1f;
        public SpawnParameter RespawnParameter;

        public Vector3 SpawnPosition
        {
            get { return m_spawnPosition; }
        }

        private float m_currentShotWait;
        private float m_maxHealth;
        private Vector3 m_spawnPosition;
        private bool m_onFirstEnemy = false;

        void Awake()
        {
            AttractionCollider.SetRadius(AttractionRadius);
            m_maxHealth = HealthPoints;
            m_currentShotWait = ShootPauseInSeconds; // so enemy could shoot at start
            if (!m_onFirstEnemy)
            {
                StaticData.OnGameOver += () => { HeadMaterial.color = StaticData.EnemyHeadColor; };
                AttractionCollider.OnAttractionGained += () => { HeadMaterial.color = StaticData.EnemyHeadColor; };
                HeadMaterial.color = Color.white;
                m_onFirstEnemy = !m_onFirstEnemy;
            }
        }

        void OnEnable()
        {
            m_spawnPosition = this.transform.position; // poolmanager sets position before setactive
        }
        
        void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.tag == "Projectile")
            {
                Projectile projectile = collision.gameObject.GetComponent<Projectile>();
                HealthPoints -= projectile.Damage;
                if (HealthPoints <= 0)
                {
                    PoolManager.Destroy(this.gameObject);
                    EnemySpawner.Instance.AddRespawnable(this);
                    reset();
                }
            }
        }

        void Update()
        {
            if (AttractionCollider.FollowPlayer && AttractionCollider.Player)
            {
                FollowPlayer();
                ShootPlayer();
            }
        }

        private void FollowPlayer()
        {
            this.transform.LookAt(AttractionCollider.Player.transform);
            var directionVectorToPlayer = AttractionCollider.Player.transform.position - this.transform.position;
            if (directionVectorToPlayer.magnitude >= MaximumProximityDistance)
            {
                Vector3 newPos = this.transform.position + this.transform.forward * MovementSpeed * Time.deltaTime;
                this.transform.position = new Vector3(newPos.x, this.transform.position.y, newPos.z);
            }
        }

        private void ShootPlayer()
        {
            if (!StaticData.IsGameOver)
            {
                m_currentShotWait += Time.deltaTime;
                if (m_currentShotWait >= ShootPauseInSeconds)
                {
                    GameObject go = PoolManager.Instantiate(ProjectilePrefab, ProjectileExit.position, ProjectileExit.rotation);
                    go.GetComponent<Projectile>().Fire(this.transform.forward, ForceMode.Impulse);
                    m_currentShotWait = 0;
                }
            }
        }

        private void reset()
        {
            AttractionCollider.Reset();
            m_currentShotWait = 0;
            HealthPoints = m_maxHealth;
        }
    }
}
