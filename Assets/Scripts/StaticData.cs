﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class StaticData
    {

        static StaticData()
        {
            m_isGameOver = false;
        }

        public static bool IsGameOver
        {
            get { return m_isGameOver; }
            set
            {
                m_isGameOver = value;
                if (value && OnGameOver != null)
                    OnGameOver();
            }
        }

        public static System.Action OnGameOver;
        private static bool m_isGameOver;

        public static Color EnemyHeadColor
        {
            get { return IsGameOver ? Color.green : Color.red; }
        }
    }
}
